<?php

namespace Chatter\Middleware;

class Logging {
    public function __invoke($request,$response,$next){
        //before route 
        error_log($request->getMethod()."--".$request->getUri()."\r\n" // כותב את הסטרינג לתוך קובץ אם הקובץ הזה לא קיים הוא יוצר אותו
        ,3,"log.txt") ;

    $response = $next($request,$response); // במקרה הזה זה אומר לנו לעבור למידלוואר הבא, במקרה זה אין לנו
        //after route
        return $response;
    }

}