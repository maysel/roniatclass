<?php

require "bootstrap.php";
use Chatter\Models\Message; //create use action so he can use the dep we created
use Chatter\Models\User;
// nov 19
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging()); // הוספנו יוז למעלה, והוספנו דרך אדד, כלומר הוספנו שכבה של מידלוואר

$app->get('/hello/{name}', function($request, $response,$args){ //route
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{number}', function($request, $response,$args){ //route
   return $response->write('your customer number is '.$args['number']);
});
$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){ //route
   return $response->write('customer '.$args['cnumber'].' purchased product number '.$args['pnumber'] );
});

//messages - at the class

$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();
    $payload=[];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=> $msg->body,
            'user_id'=> $msg->user_id,
            'created_at'=> $msg->created_at ];
        

    }
    return $response->withStatus(200)->withJson($payload);
});



$app->post('/messages', function($request, $response,$args){ //route
   $message = $request->getParsedBodyParam('message',''); // take the data that get from the request into message
   $userid = $request->getParsedBodyParam('userid','');
   $_message = new Message(); // create an object from masg type
   $_message->body = $message; // the body of the message iw what that arriaved from post
   $_message->user_id = $userid; 
   $_message->save();
   if ($_message->id){ // if the msg has id and it save as is supose to be
    $payload = ['message_id'=>$_message->id];
   return $response->withStatus(201)->withJson($payload);
   }else{
       return $response->withStatus(400);
   }
});


$app->delete('/messages/{message_id}', function($request, $response,$args){ //route
   $_message = Message::find($args['message_id']);
   $_message->delete();
   if($_message->exists){
       return $response->withStatus(400);
   }else{
       return $response->withStatus(200);
   }
});

// class nov 19 -> PUT, is like update..
$app->put('/messages/{message_id}', function($request, $response,$args){ //route will need both - message and msg id
   $message = $request->getParsedBodyParam('message',''); // , משכנו את ההודעה מהיואראל
   $_message = Message::find($args['message_id']);   //נרצה למשוך את ההודעה מהמסד נתונים באמצעות המספר שמגיע ביואראל
   //die ("Message id is: ".$_message->id); // בדיקה שעובד
   $_message->body = $message; // דורסים ערך ישן ושמים ערך חדש,השורה שבה מתחולל העדכון
   if($_message->save()){
       $payload = ['message_id'=>$_message->id, "Result"=>"The message has been updated successfully (: "];
        return $response->withStatus(200)->withJson($payload);
   }else{
       return $response->withStatus(400);
   }
});

$app->post('/messages/bulk', function($request, $response,$args){ //route
   $payload = $request->getParsedBody();// מוציא את כל מה שיש בו, ומה שיש בו אמור להיות גייסון
   
   Message::insert($payload); // שומרים במסד הנתונים כל מה שהגיע מהגייסון
   return $response->withStatus(201)->withJson($payload);
});














//users - home work

$app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all();
    $payload=[];
    foreach($users as $msg){
        $payload[$msg->id] = [
            'username'=> $msg->username,
            'email'=> $msg->email,];
        

    }
    return $response->withStatus(200)->withJson($payload);
});


$app->delete('/users/{id}', function($request, $response,$args){ //route
   $user = User::find($args['id']); // find the ralvent by id
   $user->delete();
   if($user->exists){
       return $response->withStatus(400);
   }else{
       return $response->withStatus(200);
   }
});

$app->run();




//hw3
$app->get('/users', function($request, $response,$args){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->post('/users', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
   $_user = new User();
   $_user->username = $username;
   $_user->save();
   if($_user->id){
       $payload = ['user id: '=>$_user->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']);
    $user->delete();
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});


$app->run();